#!/bin/bash
bug=$1
config=$2
repeats=$3
vmname="${bug}-${config}"

# ip=$2
# size="Standard_A4_v2"
size="Standard_F4"
#size="Standard_D1_v2"

# provision the VM
az group create --name "${vmname}" --location eastus
az vm create  --image UbuntuLTS \
              --authentication-type ssh \
              --ssh-key-value "$(cat ~/.ssh/id_rsa.pub)" \
              --storage-sku Standard_LRS \
              --size "${size}" \
              -n "${vmname}" \
              -g "${vmname}"
ip=$(az vm list-ip-addresses --name "${vmname}" | jq .[0].virtualMachine.network.publicIpAddresses[0].ipAddress | sed 's/"//g')

# prepare
scp -o StrictHostKeyChecking=no prepare-azure.sh chris@${ip}:. && \
ssh -o StrictHostKeyChecking=no chris@${ip} ./prepare-azure.sh && \
scp -o StrictHostKeyChecking=no secrets/mailgun.key chris@${ip}:. && \
scp -o StrictHostKeyChecking=no -r "${HOME}/.azure" chris@${ip}:. && \
scp -o StrictHostKeyChecking=no -r setup chris@${ip}:. && \
scp -o StrictHostKeyChecking=no runner.sh chris@${ip}:. && \
scp -o StrictHostKeyChecking=no baseline.sh chris@${ip}:. && \
scp -o StrictHostKeyChecking=no collect.sh chris@${ip}:. && \
scp -o StrictHostKeyChecking=no run-azure.sh chris@${ip}:. && \
ssh -o StrictHostKeyChecking=no chris@${ip} ./run-azure.sh "${bug}" "${config}" "${repeats}"
