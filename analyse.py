#!/usr/bin/python3
import os
import json
import csv
import random
import numpy as np
import matplotlib.pyplot as plt

from tabulate import tabulate

SEEDS = range(10)
BUGS = [
    'siemens-replace-one',
    'siemens-replace-two',
    'siemens-replace-three',
    'siemens-printtokens-one',
    'siemens-printtokens-two',
    'siemens-printtokens-three',
    'siemens-printtokens2-one',
    'siemens-printtokens2-two',
    'siemens-printtokens2-three',
    'siemens-schedule-one',
    'siemens-schedule-two',
    'siemens-schedule-three',
#    'siemens-schedule2-one',
#    'siemens-schedule2-two',
#    'siemens-schedule2-three',
    'siemens-totinfo-one',
    'siemens-totinfo-two',
    'siemens-totinfo-three',
    'siemens-tcas-one',
    'siemens-tcas-two',
    'siemens-tcas-three'
]
CONFIGS = [
    'standard',
    'shadow',
    'restricted',
    'double-tournament',
    'neutral',
    'neutral_negative'
]


"""
Saves a results table to a given location.
"""
def to_csv(data, fn):
    with open(fn, "w") as f:
        writer = csv.writer(f)
        writer.writerows(data)


"""
Determines whether a given run was successful, and, if relevant, the number of
unique evaluations required to find a solution for that run. Returns the result
as a pair of the form (success, evaluations)
"""
def summarise_run(bug, config, seed):
    file_path = "results/{}-{}-{}.json".format(bug, config, seed)
    with open(file_path, "r") as f:
        results = json.load(f)
        success = results['termination']['type'] == 'found_acceptable_solution'
        if success:
            evaluations = results['evaluator']['patches']
        else:
            evaluations = None

    # generate some fake data
    # success = random.choice([True, False])
    # evaluations = random.randint(1, 400) if success else None
    return (success, evaluations)


"""
Returns a summary of the performance of a particular configuration for a given
bug. This summary is provided as a pair. The first element within the pair
describes the fraction of runs that were successful. The second element gives
a list of the number of unique candidate evaluations required for each
successful run.
"""
def summarise_bug_config(bug, config):
    runs = len(SEEDS)
    successes = 0
    evaluations = []
    for seed in SEEDS:
        (success, evals) = summarise_run(bug, config, seed)
        successes += int(success)
        if not evals is None:
            evaluations.append(evals)
    successes /= runs
    return (successes, evaluations)


"""
Provides a summary of the performance of a given configuration in terms of its
performance across each of the bug scenarios. Returns a summary in the form of
a dict, where each entry gives the performance for a particular bug.

:see summarise_bug_config
"""
def summarise_config(config):
    return {b: summarise_bug_config(b, config) for b in BUGS}


"""

:see https://matplotlib.org/2.0.0/examples/pylab_examples/barchart_demo.html
"""
def plot_success_rate(success_rate):
    a_label = success_rate[0][1]
    b_label = success_rate[0][2]
    success_rate = success_rate[1:]
    num_bugs = len(success_rate)

    bug_labels = [success_rate[i][0][8:] for i in range(num_bugs)]
    a_sr = [success_rate[i][1] for i in range(num_bugs)]
    b_sr = [success_rate[i][2] for i in range(num_bugs)]

    (fig, ax) = plt.subplots()

    #some calculation to determine the position of Y ticks labels
    width_bar = 0.5
    width_space = 0.5
    total_space = (num_bugs * 2 * width_bar) + (num_bugs - 1 * width_space)
    ind_space = 2 * width_bar
    step = ind_space/2.0
    pos = np.arange(step, total_space + width_space, ind_space + width_space)

    a_rects = plt.barh(pos - step + width_bar,      a_sr, color='g', label=a_label) 
    b_rects = plt.barh(pos - step + 2 * width_bar,  b_sr, color='r', label=b_label)

    plt.xlabel('Success Rate')
    plt.ylabel('Scenario')
    plt.title('Success Rate ({} vs. {})'.format(a_label, b_label))
    plt.yticks(index, bug_labels)
    plt.legend()

    plt.tick_params(axis='both', which='major', labelsize=8)
    plt.tick_params(axis='both', which='minor', labelsize=8)

    # plt.tight_layout()
    plt.show()


"""
Compares the performance of two configurations, A and B.
"""
def compare_configs(a, b):
    num_bugs = len(BUGS)
    a_summary = summarise_config(a)
    b_summary = summarise_config(b)

    # effectiveness -- how many bugs did the configuration solve?
    a_effectiveness = sum(1 for (successes, _) in a_summary.values() if successes > 0)
    a_effectiveness = (a_effectiveness / num_bugs) * 100
    b_effectiveness = sum(1 for (successes, _) in b_summary.values() if successes > 0)
    b_effectiveness = (b_effectiveness / num_bugs) * 100

    print("Effectiveness ({} vs. {}):".format(a, b))
    print("\t{}: {:0.2f}%".format(a, a_effectiveness))
    print("\t{}: {:0.2f}%".format(b, b_effectiveness))
    print("")

    # table: success rate
    # TODO: calculate p and A
    success_rate = [["Bug", a, b]]

    for bug in BUGS:
        (bug_success_a, _) = a_summary[bug]
        (bug_success_b, _) = b_summary[bug]
        success_rate.append([bug, bug_success_a, bug_success_b])

    print("Success Rate ({} vs. {})".format(a, b))
    print(tabulate(success_rate, headers="firstrow"))
    print("")
    to_csv(success_rate, "tables/success_rate_{}_vs_{}.csv".format(a, b))

    # plot success rate per bug as a bar chart
    # plot_success_rate(success_rate)

    # efficiency
    # TODO: calculate p and A
    efficiency_median = [["Bug", a, b]]
    efficiency_mean = [["Bug", a, b]]

    for bug in BUGS:
        (_, efficiency_a) = a_summary[bug]
        (_, efficiency_b) = b_summary[bug]


        if efficiency_a == []:
            mean_a = 0
            median_a = 0
        else:
            mean_a = np.mean(efficiency_a)
            median_a = np.median(efficiency_a)

        if efficiency_b == []:
            mean_b = 0
            median_b = 0
        else:
            mean_b = np.mean(efficiency_b)
            median_b = np.median(efficiency_b)

        efficiency_median.append([bug, median_a, median_b])
        efficiency_mean.append([bug, mean_a, mean_b])

    print("Mean NCP ({} vs. {})".format(a, b))
    print(tabulate(efficiency_median, headers="firstrow"))
    print("")
    to_csv(efficiency_median, "tables/efficiency_median_{}_vs_{}.csv".format(a, b))

    print("Median NCP ({} vs. {})".format(a, b))
    print(tabulate(efficiency_mean, headers="firstrow"))
    print("")
    to_csv(efficiency_mean, "tables/efficiency_mean_{}_vs_{}.csv".format(a, b))

    # plot efficiency per bug as a series of box plots (on the same axis)

"""
Analyses the raw data for this experiment
"""
def analyse():
    os.makedirs("graphs", exist_ok=True)
    os.makedirs("tables", exist_ok=True)

    compare_configs('standard', 'neutral_negative')


if __name__ == "__main__":
    analyse()
