#!/bin/bash
HERE=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)

bug=$1
setup=$2
seed=$3

docker create --log-driver none \
  -v /opt/genprog3 \
  --name genprog \
  christimperley/genprog
docker run --rm -i \
	--log-driver none \
	--env EXTRA_PATH='/opt/genprog3' \
  --env PYTHIA_PLATFORM_SCALE='2.5' \
	-v ${HERE}/runner.sh:/experiment/runner.sh \
	-v ${HERE}/setup:/experiment/setup \
  -v ${HERE}/results:/experiment/results \
  -v ${HERE}/cache:/experiment/cache \
	--volumes-from genprog \
	--name rbox \
	--entrypoint /experiment/runner.sh \
	christimperley/repairbox:${bug} ${setup} ${seed}

sudo chown -R ${USER}:$(id -gn ${USER}) results
sudo chown -R ${USER}:$(id -gn ${USER}) cache
docker rm genprog
