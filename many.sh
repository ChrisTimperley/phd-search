#!/bin/bash
function run(){
  program=$1
  size=$2
  alg=$3
  ./remote-azure.sh "siemens-${program}-${size}" "${alg}-${size}" 5 > /dev/null && \
    echo "Running: ${program}-${size} (${alg})" || \
    echo "Failed to start: ${program}-${size} (${alg})"
}

function run_all(){
  program=$1
  size=$2
  ./remote-azure.sh "siemens-${program}-${size}" all 10 > /dev/null && \
    echo "Running: ${program}-${size} (all)" || \
    echo "Failed to start: ${program}-${size} (all)"
}

#run_all schedule one
run_all schedule two
run_all schedule three

run_all schedule2 one
run_all schedule2 two
run_all schedule2 three

run_all printtokens one
run_all printtokens two
run_all printtokens three

run_all printtokens2 one
run_all printtokens2 two
run_all printtokens2 three

run_all totinfo one
run_all totinfo two
run_all totinfo three

run_all tcas one
run_all tcas two
run_all tcas three

run_all replace one
run_all replace two
run_all replace three

# run schedule one genetic
# run schedule one greedy
# run schedule two genetic
# run schedule two greedy
# run schedule three genetic
# run schedule three greedy

# run schedule2 one genetic
# run schedule2 one greedy
# run schedule2 two genetic
# run schedule2 two greedy
# run schedule2 three genetic
# run schedule2 three greedy

# run printtokens one genetic
# run printtokens one greedy
# run printtokens two genetic
# run printtokens two greedy
#run printtokens three genetic
# run printtokens three greedy

#run printtokens2 one genetic
#run printtokens2 one greedy
#run printtokens2 two genetic
#run printtokens2 two greedy
#run printtokens2 three genetic
#run printtokens2 three greedy

# run totinfo one genetic
# run totinfo one greedy
# run totinfo two genetic
# run totinfo two greedy
# run totinfo three genetic
# run totinfo three greedy

# run tcas one genetic
# run tcas one greedy
#run tcas two genetic
#run tcas two greedy
#run tcas three genetic
#run tcas three greedy
#
#run replace one genetic
#run replace one greedy
#run replace two genetic
#run replace two greedy
#run replace three genetic
#run replace three greedy
