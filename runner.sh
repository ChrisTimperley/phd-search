#!/bin/bash
export PATH="${EXTRA_PATH}:${PATH}"
setup=$1
seed=$2

cd /experiment

# take ownership of the cache and results
sudo chown -R $(whoami) cache
sudo chown -R $(whoami) results

jq -s add problem.json setup/${setup}.json > setup.json && \
genprog setup.json \
        --seed ${seed} \
        --threads $(nproc) \
        --cache "/experiment/cache/${SCENARIO_NAME}.cache" \
        --coverage /experiment/coverage \
        --output_dir /experiment/results \
        --output_name "${SCENARIO_NAME}-${setup}-${seed}.json"
