#!/bin/bash

# install Docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"
sudo apt-get update
sudo apt-get install -y docker-ce
sudo gpasswd -a ${USER} docker

# install Azure CLI
echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ wheezy main" | \
	sudo tee /etc/apt/sources.list.d/azure-cli.list
sudo apt-key adv --keyserver packages.microsoft.com --recv-keys 417A0893
sudo apt-get install -y apt-transport-https
sudo apt-get update && sudo apt-get install -y azure-cli

# disable core dumps
sudo mkdir -p /etc/systemd/coredump.conf.d
echo "[Coredump]\nStorage=none" > custom.conf
sudo mv custom.conf /etc/systemd/coredump.conf.d
sudo systemctl daemon-reload

# mount results
mkdir results
sudo mount -t cifs //cstdata.file.core.windows.net/results results -o vers=2.1,username=cstdata,password=W5kr64Im6QtW3Loh0KHIP5lEhUR1FGzwx8FnEODWJMLHx497OxrY1E37pJ8xuLeabuHR7iDM9/XyrDT/jHHb/w==,dir_mode=0777,file_mode=0777

# mount cache
mkdir cache 
# sudo mount -t cifs //cstdata.file.core.windows.net/cache cache -o vers=2.1,username=cstdata,password=W5kr64Im6QtW3Loh0KHIP5lEhUR1FGzwx8FnEODWJMLHx497OxrY1E37pJ8xuLeabuHR7iDM9/XyrDT/jHHb/w==,dir_mode=0777,file_mode=0777
