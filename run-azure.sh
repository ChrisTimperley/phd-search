#!/bin/bash
HERE=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
pushd "${HERE}"

bug=$1
config=$2
repeats=$3
nohup ./collect.sh "${bug}" "${config}" "${repeats}" > run.out 2> run.err < /dev/null &
