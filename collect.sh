#!/bin/bash
bug=$1
config=$2
seeds=$3

if [ "${config}" == "all" ]; then
  for setup in "standard" "shadow" "restricted" "double-tournament" "neutral" "neutral_negative"
  do
    for (( seed=0; seed<seeds; seed++ )); do
      ./baseline.sh "${bug}" "${setup}" "${seed}"
    done
  done
else
  for (( seed=0; seed<seeds; seed++ )); do
    ./baseline.sh "${bug}" "${config}" "${seed}"
  done
fi

  # notify me!
curl -s --user "api:key-$(cat mailgun.key)" \
    https://api.mailgun.net/v3/sandbox929c1b44222045d5bcee2b37ca48d3c6.mailgun.org/messages \
		-F from='Mailgun Sandbox <postmaster@sandbox929c1b44222045d5bcee2b37ca48d3c6.mailgun.org>' \
		-F to='Chris <christimperley@googlemail.com>' \
    -F subject="Job Complete: ${bug} (${config})" \
		-F text='Finished'

# kill the machine
VMNAME=$(cat /etc/hostname)
az group delete --yes --name "${VMNAME}"
